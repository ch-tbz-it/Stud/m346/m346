![TBZ Logo](../x_gitres/tbz_logo.png)


[TOC]

# KN06: Skalierung

Beachten Sie die [allgemeinen Informationen zu den Abgaben](https://gitlab.com/ch-tbz-it/Stud/m346/m346/-/blob/main/Abgaben.md).

In dieser Kompetenz werden Sie eine Java oder .NET Applikation installieren und Instanzen horizontal, vertikal und automatisiert skalieren.  **[Lesen Sie sich in die Theorie ein, auch in die Dokumente in der Quelle](https://gitlab.com/ch-tbz-it/Stud/m346/m346/-/blob/main/CloudComputing.md#skalierung)**.

## Lernziele

- Sie nehmen ein Web API in Betrieb und erstellen dafür einen Service 
- Sie verwenden einen Reverse Proxy um den Port auf den korrekten Serviceport umzuleiten.
- Sie verwenden für die Datenbank einen SAAS Service, anstatt eine eigene Datenbank
- Sie lernen wie Sie vertikal und horizontal Skalieren
- Sie verwenden einen Load Balancer um die horizontale Skalierung zu erreichen
- Sie verwenden einen AWS Service (automatischer Skalierer) um fehlerhafte Instanzen einfach zu ersetzen.



## A) Installation App (50%)

Sie werden nun eine .NET oder Java Applikation in Betrieb nehmen und dabei auf einen SAAS Service für die Datenbank zurückgreifen,.

**a) MongoDB als Datenbank einrichten**

Diese neue Applikation verwendet MongoDB als Datenbank und wird mit MongoDb Atlas gehostet (**SaaS**). Sie benötigen also  daher keinen zusätzlichen Datenbank-Server. Richten Sie sich ein [kostenloses Konto ein](https://www.mongodb.com/). Stellen Sie sicher, dass auf diese Datenbank zugegriffen werden kann, indem Sie den entsprechenden IP Range angeben. Den gleichen Schritt mussten Sie für MariaDB ausführen in KN03. 

![atlas-1](./x_res/atlas-1.png)

Ihr Cloud-Init Skript für den Webserver wird via Shell (MongoDb Shell) auf die Datenbank zugreifen. Falls Sie ein graphisches UI zusätzlich möchten, verwenden Sie MongoDB Compass. Mit der Option *Browse Collections* haben Sie aber auch die Möglichkeit Ihre Datenbank(en) via Browser anzuschauen.

![atlas-1](./x_res/atlas-2.png)



**b) Web Server installieren auf AWS**

Installieren Sie nun wieder einen Web-Server, aber nun mit einer komplexeren Applikation/Web API. Im Ordner für [KN06](https://gitlab.com/ch-tbz-it/Stud/m346/m346/-/tree/v4.1/KN06) finden Sie Cloud-init Dateien. Entscheiden Sie sich für die **Java oder .Net** Applikation. 

**Java Version**

- Zugriffspunkt: http://Ihre-IP/swagger-ui.html
- Läuft intern auf dem Port 5001

**.NET Version**

- Zugriffspunkt: http://Ihre-IP/swagger/
- Läuft intern auf dem Port 5000

Bisher hatten wir Apache als Web-Server verwendet. Nun werden wir Nginx einsetzen. Sie möchten ja nicht, dass ihre Applikation nur über den Port 5001, resp 5000 aufgerufen werden kann, sondern über den normalen Port 80. Nginx wird daher als [Reverse Proxy](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/) eingesetzt, welcher die Anfragen weiterleitet. Sie finden die entsprechende Konfiguration im Cloud-Init.

In AWS, verwenden Sie die Standardeinstellungen für die Instanz, **ohne** statische IP. 

Verwenden Sie die vorgegebene Cloud-Init Konfiguration. **Achtung**:  Sie müssen gewisse Platzhalter ersetzen (markiert mit den Zeichen `[[....]]` ). 

> **Wichtig:** Es kann sein, dass Sie ein paar Minuten warten müssen bis alles installiert ist. 

**Abgaben**:

- Kurze Erklärung in **eigenen Worten** was ein Reverse Proxy ist.
- Screenshot der zeigt, dass Sie die Swagger-URL aufrufen können. 
- Screenshot der zeigt, dass sie den Endpoint `products` (Java), resp`GetProducts` (.NET) aufrufen können (via Swagger) und auch ein korrektes Resultat bekommen.
- Screenshot einer der MongoDB Collections mit Auszug aus dem Inhalt. 
- Schauen Sie sich das Cloud-Init genau an. Welche(r) Teil(e) macht/machen hier überhaupt keinen Sinn in einer produktiven Umgebung?
- Die Lehrperson wird Sie zum Cloud-Init befragen, stellen Sie sicher, dass Sie die Inhalte verstehen.



## B) Vertikale Skalierung  (10%)

Skalieren Sie nun ihren Webserver und erweitern Sie die Disk auf **20GB**. **Erklären** Sie die Schritte die notwendig waren. Geht dies im laufenden Betrieb? 

Skalieren Sie nun auch weitere Ressourcen und Verwenden Sie die den Instanztyp **t2.medium**. **Erklären** Sie die Schritte, die notwendig waren. Geht dies im laufenden Betrieb?

**Abgaben:**

- Vorher-Nachher Screenshots der Instanz-Ressourcen
- Erklärungen.



## C) Horizontale Skalierung (20%)

Erstellen Sie einen Load Balancer und fügen Sie diesem **zwei** Instanzen des Web Servers hinzu. Suchen Sie **selbstständig** nach Ressourcen. Überdenken Sie ihre anderen Objekte wie *statischen IPs, Sicherheitsgruppen*, etc. Was macht nun Sinn in ihrer neuen Umgebung? Richten Sie Ihre Umgebung entsprechend ein.

Rufen Sie nun die Load-Balancer URL auf und zeigen Sie, dass Swagger und der Endpoint auch mit dieser URL funktioniert. Sie haben nun eine Domain und keine IP mehr. Ihr Loadbalancer ist nun mit einem DNS verfügbar wie z.B. x.elb.amazonaws.com. Nehmen sie an, dass diese Applikation unter der URL *app.tbz-m346.ch* verfügbar sein soll. Wie müssten Sie den DNS konfigurieren, damit dies funktioniert?

**Abgaben**:

- Erklärungen zum DNS
- Screenshot des Swagger-Aufrufs über die LoadBalancer URL (mit sichtbarer URL)
- Seien Sie bereit Auskunft zu erteilen über ihr Vorgehen und die erstellten Objekte wie *Load Balancer, Target Group, Health Check, IPs, Sicherheitsgruppen, Listener u. a.*



## D) Auto Scaling (20%)

Erstellen Sie ein Auto-Scaling, welches mit ihrem Load Balancer zusammenarbeitet. Als Ziel sollen jeweils 2 Instanzen verfügbar sein, maximal aber 5. Bearbeiten Sie diese Aufgabe aber nur, wenn die *Health Checks* bei C) grün sind.

Testen Sie, ob Ihre Autoskalierung funktioniert, indem Sie die bestehenden Webserver einfach herunterfahren. 

**Abgaben:** 

- Die Lehrperson wird die Umgebung genauer anschauen.
- Seien Sie bereit Auskunft zu erteilen über ihr Vorgehen und die erstellten Objekte wie *Template, Health Checks, u.a.*



## Leitfragen / Checkpoints

- Ich kann erklären was ein Reverse Proxy ist und wie er in Zusammenhang steht mit einer Applikation, die auf einem eigenen Port läuft. 
- Ich kann einen Service auf Ubuntu aktivieren, starten, stoppen, etc. Ich weiss ausserdem wie ich eine Java oder .NET Applikation starten kann und welche Tools dazu notwendig sind.
- Ich kenne den Zusammenhang zwischen der Applikation und der Konfigurationsdatei. 
- Ich kann eine Datenbank mit MongoDB Atlas einrichten und die Sicherheitsregel so festlegen, dass die Datenbank geschützt ist.
- Ich kann vertikale und horizontale Skalierung erklären und mit Beispielen ausführen. Ausserdem kann ich mit den AWS Services und Tools die beiden Skalierungsarten umsetzen.
- Ich kann Cloud-Init mit Hilfe einer Vorlage auch für kompliziertere Umgebung verwenden.





